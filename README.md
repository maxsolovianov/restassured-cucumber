# Cucumber API test framework

This is test framework for mobile API with using Cucumber

# How to run all tests
```bash
cd ~/restassured-cucumber
mvn clean install -q
```

# How to run (bash script example, please see as raw)

```bash
#! /bin/bash
cd ~/restassured-cucumber
mvn clean install -q \
-D"cucumber.options= --tags @mobile --tags ~@ignore"
```

# Reporting:

1. /restassured-cucumber/target/cucumber.json - for Jenkins plugin

2. /restassured-cucumber/target/cucumber-html-reports/overview-features.html

