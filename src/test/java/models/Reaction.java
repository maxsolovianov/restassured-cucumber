package models;

public class Reaction {

    private int value;
    private String label;
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public final int getValue() {
        return this.value;
    }

    public final String getLabel() {
        return  this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
