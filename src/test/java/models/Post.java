package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.google.gson.annotations.SerializedName;
import java.util.List;

// ignore any non declared property when don't want to write the whole mapping
@JsonIgnoreProperties(ignoreUnknown = true)
public class Post {

    private boolean isTrending;

//     [ ] json objects
//    @SerializedName("reactions")

    private List<Reaction> reactions;

    public boolean isTrending() {
        return isTrending;
    }

    public final List<Reaction> getReactions() {
        return this.reactions;
    }

    public void setReactions(List<Reaction> reactions) {
        this.reactions = reactions;
    }

    public void setTrending(boolean trending) {
        isTrending = trending;
    }
}
