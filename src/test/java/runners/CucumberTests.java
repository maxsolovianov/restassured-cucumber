package runners;

import configs.TestConfiguration;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.springframework.test.context.ContextConfiguration;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = "json:target/cucumber.json",
		glue = {"stepdefs"},
		features = {"src/test/features"},
		tags = {"~@ignore"})
@ContextConfiguration(classes = TestConfiguration.class)
public class CucumberTests {}
