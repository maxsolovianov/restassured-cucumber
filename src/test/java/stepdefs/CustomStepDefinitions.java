package stepdefs;
import commons.CommonData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import configs.TestConfiguration;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

@ContextConfiguration(classes = TestConfiguration.class)
public class CustomStepDefinitions {

    @Autowired
    private CommonData stepData;

    @Given("a news exists with an id of (.*)$")
    public void a_news_exists_with_id(String id) {
        stepData.setRequest(given().param("id", id));
    }

    @Then("the body matches the following Json Schema in classpath (.*)$")
    public void the_body_matches_the_following_Json_Schema_in_classpath(String fileName) {
        stepData.getResponse().then().body(matchesJsonSchemaInClasspath(fileName));
    }

    @Given("a list of categories exists on server$")
    public void a_list_exists_on_server() {
        stepData.setRequest(given());
    }

    @Given("a posts exists on server$")
    public void a_objects_exists_on_server() {
        stepData.setRequest(given());
    }
}


