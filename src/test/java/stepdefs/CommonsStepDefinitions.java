package stepdefs;

import commons.CommonData;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.xml.XmlPath;
import models.Post;
import models.Reaction;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import configs.TestConfiguration;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;

@ContextConfiguration(classes = TestConfiguration.class)
public class CommonsStepDefinitions {

    @Autowired
    private CommonData stepData;

    @When("I won't follow a redirect for getting response of the original request$")
    public void follow_a_redirect_is_false() {
        stepData.setRequest(stepData.getRequest().redirects().follow(false));
    }

    @When("a user go to (.*)$")
    public void a_user_go_to(String url) {
        stepData.setResponse(stepData.getRequest().when().get(url));
//        System.out.println("response: " + stepData.getResponse().asString());
    }

    @When("a user retrieves the html document includes the following values$")
    public void a_user_retrieves_the_html_document_includes_the_following_values(Map<String, String> map) {
        stepData.setDocument(new XmlPath(XmlPath.CompatibilityMode.HTML, stepData.getResponse().asString()));
//        System.out.println("document: " + stepData.getDocument().prettyPrint());
        for (Map.Entry<String, String> field : map.entrySet()) {
            Assert.assertEquals(field.getValue(), stepData.getDocument().getString(field.getKey()));
//            System.out.println(field.getValue());
//            System.out.println(stepData.getDocument().getString(field.getKey()));
        }
    }

    @When("a user retrieves the data from service (.*)$")
    public void a_user_retrieves_the_data_from_service(String url) {
        stepData.setResponse(stepData.getRequest().when().get(url));
//        System.out.println("response: " + stepData.getResponse().prettyPrint());
    }

    @Then("the http status code is (\\d+)$")
    public void the_status_code_is(int statusCode) {
        stepData.setJson(stepData.getResponse().then().statusCode(statusCode));
    }

    @Then("the header (.*) contains (.*)$")
    public void the_header_contains(String header, String content) {
        Assert.assertEquals(content, stepData.getResponse().getHeader(header));
//        System.out.println(stepData.getResponse().getHeaders());
    }

    @And("response includes the following$")
    public void response_equals(Map<String, String> responseFields) {
        for (Map.Entry<String, String> entry : responseFields.entrySet()) {
            if (StringUtils.isNumeric(entry.getValue())) {
                stepData.getJson().body(entry.getKey(), equalTo(Integer.parseInt(entry.getValue())));
            } else {
                stepData.getJson().body(entry.getKey(), equalTo(entry.getValue()));
            }
        }
    }

    @And("a user retrieves the posts with using such parameters from (.*)")
    public void retrieves_data_from_service_using_parameters(String url, Map<String, String> params) {
        stepData.setResponse(stepData.getRequest().params(params).when().get(url));
    }

    @And("response includes no more than (.*) trending (.*)$")
    public void get_no_more_then_5_trending_post(Integer number, String entity) {
        List<Post> posts = stepData.getResponse().then().extract().jsonPath().getList(entity, Post.class);
        Assert.assertTrue(!posts.isEmpty());
        int countOfTrending = 0;
        for (Post post : posts) {
            if (post.isTrending()) {
                ++countOfTrending;
            }
        }
//        System.out.println("Count of Trending: " + countOfTrending);
        Assert.assertTrue(countOfTrending <= number);
    }

    @When("user send reaction key index (.*) for postId (.*) to service (.*)$")
    public void user_retrieves_the_news_with_reactions(Integer keyIndex, String postId, String url) {
//        stepData.getResponse().getBody().prettyPrint();
        Post returnedPost = stepData.getResponse().getBody().as(Post.class);
        List<Reaction> reactions = returnedPost.getReactions();
        Reaction reaction = null;
        if (!reactions.isEmpty()) {
            reaction = reactions.get(keyIndex);
        }
        stepData.setRequest(given().param("postId", postId).param("reaction", reaction.getKey()));
        stepData.setResponse(stepData.getRequest().when().get(url));
//        System.out.println("response: " + stepData.getResponse().asString());
    }

    @And("response includes the (.*) number of (.*)$")
    public void response_find_all_objects(Integer number, String entity) {
        stepData.getJson().body(entity + ".findall.size()", equalTo(number));
    }

    @And("response includes the following list by key (.*)$")
    public void response_contains_list_in_any_order_by_key(String key, List<String> args) {
        stepData.getJson().body(key, Matchers.equalTo(args));

    }

    @And("response includes the following in any order$")
    public void response_contains_in_any_order(Map<String, String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (StringUtils.isNumeric(field.getValue())) {
                stepData.getJson().body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
            } else {
                stepData.getJson().body(field.getKey(), containsInAnyOrder(field.getValue()));
            }
        }
    }
}
