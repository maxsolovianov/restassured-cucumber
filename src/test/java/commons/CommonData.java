package commons;

import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("commonDataBean")
@Scope("cucumber-glue")
public class CommonData {

    private RequestSpecification request;
    private Response response;
    private ValidatableResponse json;
    private XmlPath document;

    public void setDocument(XmlPath document) { this.document = document; }
    public XmlPath getDocument() { return document; }
    public void setResponse(Response response) {
        this.response = response;
    }
    public void setRequest(RequestSpecification request) {
        this.request = request;
    }
    public void setJson(ValidatableResponse json) {
        this.json = json;
    }
    public RequestSpecification getRequest() {
        return request;
    }
    public ValidatableResponse getJson() {
        return json;
    }
    public Response getResponse() {
        return response;
    }
}
