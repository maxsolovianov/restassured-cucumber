package helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ListReportProvider;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;

public class CheckJsonLocally {
    public static void main(String[] args) throws IOException, ProcessingException {
        JsonNode fstabSchema = JsonLoader.fromFile(new File("input/schema3.json"));
        JsonNode good = JsonLoader.fromFile(new File("input/sample3.json"));
        JsonSchemaFactory factory = JsonSchemaFactory.newBuilder()
                .setReportProvider(new ListReportProvider(LogLevel.ERROR, LogLevel.ERROR))
                .freeze();
//        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonSchema schema = factory.getJsonSchema(fstabSchema);
        ProcessingReport report = schema.validate(good, true);
        Assert.assertTrue(report.isSuccess());
        System.out.println(report);
    }
}
