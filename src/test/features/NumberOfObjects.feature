@news_by_id
Feature: Get some number of posts depending on passed parameter into url

  @nur
  Scenario Outline: User calls web service (nur) to get a some number of posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.nur.stage.systools.pro/v3/post/list
      | category     | latest     |
      | per-page     | <expected> |
      | short        | 0          |
      | offsetPostId | 0          |
      | showTrending | 1          |
      | lang         | ru         |
    Then the http status code is 200
    And response includes the <actual> number of posts
    Examples:
      | expected | actual |
      | 1        | 1      |
      | 5        | 5      |
      | 15       | 15     |
      | 20       | 20     |
      | 50       | 50     |

  @naij
  Scenario Outline: User calls web service (naij) to get a some number of posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.naija.stage.systools.pro/v3/post/list
      | category     | latest     |
      | per-page     | <expected> |
      | short        | 0          |
      | offsetPostId | 0          |
      | showTrending | 1          |
    Then the http status code is 200
    And response includes the <actual> number of posts
    Examples:
      | expected | actual |
      | 1        | 1      |
      | 5        | 5      |
      | 15       | 15     |
      | 20       | 20     |
      | 50       | 50     |

  @yen
  Scenario Outline: User calls web service (yen) to get a some number of posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.yen.stage.systools.pro/v3/post/list
      | category     | latest     |
      | per-page     | <expected> |
      | short        | 0          |
      | offsetPostId | 0          |
      | showTrending | 1          |
    Then the http status code is 200
    And response includes the <actual> number of posts
    Examples:
      | expected | actual |
      | 1        | 1      |
      | 5        | 5      |
      | 15       | 15     |
      | 20       | 20     |
      | 50       | 50     |

  @tuko
  Scenario Outline: User calls web service (tuko) to get a some number of posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.tuko.co.stage.systools.pro/v3/post/list
      | category     | latest     |
      | per-page     | <expected> |
      | short        | 0          |
      | offsetPostId | 0          |
      | showTrending | 1          |
    Then the http status code is 200
    And response includes the <actual> number of posts
    Examples:
      | expected | actual |
      | 1        | 1      |
      | 5        | 5      |
      | 15       | 15     |
      | 20       | 20     |
      | 50       | 50     |

  @kami
  Scenario Outline: User calls web service (kami) to get a some number of posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.kami.stage.systools.pro/v3/post/list
      | category     | latest     |
      | per-page     | <expected> |
      | short        | 0          |
      | offsetPostId | 0          |
      | showTrending | 1          |
    Then the http status code is 200
    And response includes the <actual> number of posts
    Examples:
      | expected | actual |
      | 1        | 1      |
      | 5        | 5      |
      | 15       | 15     |
      | 20       | 20     |
      | 50       | 50     |

  @briefly
  Scenario Outline: User calls web service (briefly) to get a some number of posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.briefly.co.stage.systools.pro/v3/post/list
      | category     | latest     |
      | per-page     | <expected> |
      | short        | 0          |
      | offsetPostId | 0          |
      | showTrending | 1          |
    Then the http status code is 200
    And response includes the <actual> number of posts
    Examples:
      | expected | actual |
      | 1        | 1      |
      | 5        | 5      |
      | 15       | 15     |
      | 20       | 20     |
      | 50       | 50     |