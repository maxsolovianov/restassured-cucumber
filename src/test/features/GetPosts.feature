@posts
Feature: Check posts news endpoint

  @nur
  Scenario: Check posts news NUR endpoint
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.nur.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 15     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
      | lang         | ru     |
    Then the http status code is 200
    And the body matches the following Json Schema in classpath posts-schema.json

  @naij
  Scenario: Check posts news NAIJ endpoint
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.naija.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 20     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And the body matches the following Json Schema in classpath posts-schema.json

  @yen
  Scenario: Check posts news YEN endpoint
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.yen.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 20     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And the body matches the following Json Schema in classpath posts-schema.json

  @tuko
  Scenario: Check posts news TUKO endpoint
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.tuko.co.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 20     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And the body matches the following Json Schema in classpath posts-schema.json

  @kami
  Scenario: Check posts news KAMI endpoint
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.kami.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 20     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And the body matches the following Json Schema in classpath posts-schema.json

  @briefly
  Scenario: Check posts news BRIEFLY endpoint
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.briefly.co.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 20     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And the body matches the following Json Schema in classpath posts-schema.json