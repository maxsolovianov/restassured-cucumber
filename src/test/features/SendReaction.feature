@reaction
Feature: Check sending of reaction from client to server

  @nur
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 1739624
    And a user retrieves the data from service https://app.nur.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 1739624 to service https://app.nur.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And response includes the following
      | status | 100 |

  @nur
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 1739624
    And a user retrieves the data from service https://app.nur.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 1739624 to service https://app.nur.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And the body matches the following Json Schema in classpath reaction-status-schema.json

  @naij @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 1177309
    And a user retrieves the data from service https://app.naija.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 1177309 to service https://app.naija.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And response includes the following
      | status | 100 |

  @naij @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 1177309
    And a user retrieves the data from service https://app.naija.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 1177309 to service https://app.naija.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And the body matches the following Json Schema in classpath reaction-status-schema.json


  @yen @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 112024
    And a user retrieves the data from service https://app.yen.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 112024 to service https://app.yen.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And response includes the following
      | status | 100 |

  @yen @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 112024
    And a user retrieves the data from service https://app.yen.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 112024 to service https://app.yen.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And the body matches the following Json Schema in classpath reaction-status-schema.json

  @tuko @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 278090
    And a user retrieves the data from service https://app.tuko.co.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 278090 to service https://app.tuko.co.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And response includes the following
      | status | 100 |

  @tuko @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 278090
    And a user retrieves the data from service https://app.tuko.co.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 278090 to service https://app.tuko.co.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And the body matches the following Json Schema in classpath reaction-status-schema.json

  @kami @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 78079
    And a user retrieves the data from service https://app.kami.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 78079 to service https://app.kami.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And response includes the following
      | status | 100 |

  @kami @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 78079
    And a user retrieves the data from service https://app.kami.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 78079 to service https://app.kami.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And the body matches the following Json Schema in classpath reaction-status-schema.json

  @briefly @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 13491
    And a user retrieves the data from service https://app.briefly.co.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 13491 to service https://app.briefly.co.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And response includes the following
      | status | 100 |

  @briefly @ignore
  Scenario: Check sending of reaction from client to server
    Given a news exists with an id of 13491
    And a user retrieves the data from service https://app.briefly.co.stage.systools.pro/v3/post/get
    And the http status code is 200
    When user send reaction key index 0 for postId 13491 to service https://app.briefly.co.stage.systools.pro/v3/reaction/set
    Then the http status code is 200
    And the body matches the following Json Schema in classpath reaction-status-schema.json