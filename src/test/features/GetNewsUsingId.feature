@news_by_id
Feature: Get some news by id

  @nur
  Scenario: User calls web service NUR to get a some news by id
    Given a news exists with an id of 1738920
    When a user retrieves the data from service https://app.nur.stage.systools.pro/v3/post/get
    Then the http status code is 200
    And response includes the following
      | title         | В Алматы простились с внуком Олжаса Сулейменова (фото)                                              |
      | publishDate   | 2018-06-25T12:56:00+06:00                                                                           |
      | id            | 1738920                                                                                             |
      | author        | Анастасия Ходжер                                                                                    |
      | commentsCount | 0                                                                                                   |
      | url           | https://www.nur.stage.systools.pro/1738920-v-almaty-prostilis-s-vnukom-olzasa-sulejmenova-foto.html |
#      | img           | https://static-nur.akamaized.net/pogudxg912m1vrn44.r500.dd216152.jpg                                |
      | postType      | 1                                                                                                   |

  @naij @ignore
  Scenario: User calls web service NAIJ to get a some news by id
    Given a news exists with an id of 1194906
    When a user retrieves the data from service https://app.naija.stage.systools.pro/v3/post/get
    Then the http status code is 200
    And response includes the following
      | title         | Nigeria star Alex Iwobi makes an outlandish statement over Arsenal teammate Aubameyamg                             |
      | publishDate   | 2018-06-15T09:13:17+01:00                                                                                          |
      | id            | 1175324                                                                                                            |
      | author        | Oluwatomiwa Babalola                                                                                               |
      | commentsCount | 1                                                                                                                  |
      | url           | https://www.naija.stage.systools.pro/1175324-super-eagles-star-iwobi-brands-arsenal-teammate-aubameyang-flash.html |
#      | img           | https://static-naija.akamaized.net/vllkytqo9d1ed4v5s.r500.244558fe.jpg                                             |
      | postType      | 1                                                                                                                  |

  @yen @ignore
  Scenario: User calls web service YEN to get a some news by id
    Given a news exists with an id of 112311
    When a user retrieves the data from service https://app.yen.stage.systools.pro/v3/post/get
    Then the http status code is 200
    And response includes the following
      | title         | Kennedy Agyapong refuses to sit by Muntaka at Privileges Committee                                  |
      | publishDate   | 2018-07-04T07:08:24+00:00                                                                           |
      | id            | 112311                                                                                              |
      | author        | Charles Addo                                                                                        |
      | commentsCount | 0                                                                                                   |
      | url           | https://yen.stage.systools.pro/112311-ken-agyapong-refuses-sit-by-muntaka-privileges-committee.html |
#      | img           | https://static-yen.akamaized.net/3o3bpd71hekhkt20i.r500.00639aaf.jpg                                |
      | postType      | 1                                                                                                   |

  @tuko @ignore
  Scenario: User calls web service TUKO to get a some news by id
    Given a news exists with an id of 278544
    When a user retrieves the data from service https://app.tuko.co.stage.systools.pro/v3/post/get
    Then the http status code is 200
    And response includes the following
      | title         | Nairobi man demands to have custody of baby he got with sister-in-law                          |
      | publishDate   | 2018-07-04T08:39:10+03:00                                                                      |
      | id            | 278544                                                                                         |
      | author        | Michael Ollinga Oruko                                                                         |
      | commentsCount | 2                                                                                              |
      | url           | https://www.tuko.co.stage.systools.pro/278544-nairobi-man-demands-custody-baby-sister-law.html |
#      | img           | https://static-tuko.akamaized.net/0fgjhs23qiaj4p760g.r500.cc0cf8ad.jpg                         |
      | postType      | 1                                                                                              |

  @kami @ignore
  Scenario: User calls web service KAMI to get a some news by id
    Given a news exists with an id of 81461
    When a user retrieves the data from service https://app.kami.stage.systools.pro/v3/post/get
    Then the http status code is 200
    And response includes the following
      | title         | Pasikat daw! Netizens accuse Sharon Cuneta of being a show-off after sharing story of her Rolex watches     |
      | publishDate   | 2018-06-27T14:08:46+08:00                                                                                   |
      | id            | 78079                                                                                                       |
      | author        | Daniel Joseph Navalta                                                                                       |
      | commentsCount | 0                                                                                                           |
      | url           | https://kami.stage.systools.pro/78079-netizens-accuse-sharon-cuneta-a-show-sharing-story-rolex-watches.html |
#      | img           | https://static-kami.akamaized.net/0fgjhs2b57vibo1am.r500.ae1ec8ae.jpg                                       |
      | postType      | 1                                                                                                           |

  @briefly @ignore
  Scenario: User calls web service BRIEFLY to get a some news by id
    Given a news exists with an id of 17086
    When a user retrieves the data from service https://app.briefly.co.stage.systools.pro/v3/post/get
    Then the http status code is 200
    And response includes the following
      | title         | Twitter fans excited after Mamiki leaves The Queen                                            |
      | publishDate   | 2018-06-27T07:35:16+02:00                                                                     |
      | id            | 13491                                                                                         |
      | author        | Noncedo Dlamini                                                                               |
      | commentsCount | 0                                                                                             |
      | url           | https://briefly.co.stage.systools.pro/13491-twitter-fans-excited-mamiki-leaves-the-queen.html |
#      | img           | https://static-briefly.akamaized.net/0fgjhs7iopo9fc0bl.r500.ccc22a3f.jpg                      |
      | postType      | 1                                                                                             |