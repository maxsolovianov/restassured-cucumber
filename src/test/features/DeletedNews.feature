@deleted
Feature: Check deleted news endpoint

  @nur
  Scenario: Check deleted news NUR endpoint
    Given a posts exists on server
    When a user retrieves the data from service https://app.nur.stage.systools.pro/v2/post/deleted
    Then the http status code is 200
    And the body matches the following Json Schema in classpath deleted-schema.json

  @naij
  Scenario: Check deleted news NAIJ endpoint
    Given a posts exists on server
    When a user retrieves the data from service https://app.naija.stage.systools.pro/v2/post/deleted
    Then the http status code is 200
    And the body matches the following Json Schema in classpath deleted-schema.json

  @yen
  Scenario: Check deleted news YEN endpoint
    Given a posts exists on server
    When a user retrieves the data from service https://app.yen.stage.systools.pro/v2/post/deleted
    Then the http status code is 200
    And the body matches the following Json Schema in classpath deleted-schema.json

  @tuko
  Scenario: Check deleted news TUKO endpoint
    Given a posts exists on server
    When a user retrieves the data from service https://app.tuko.co.stage.systools.pro/v2/post/deleted
    Then the http status code is 200
    And the body matches the following Json Schema in classpath deleted-schema.json

  @kami
  Scenario: Check deleted news KAMI endpoint
    Given a posts exists on server
    When a user retrieves the data from service https://app.kami.stage.systools.pro/v2/post/deleted
    Then the http status code is 200
    And the body matches the following Json Schema in classpath deleted-schema.json

  @briefly
  Scenario: Check deleted news BRIEFLY endpoint
    Given a posts exists on server
    When a user retrieves the data from service https://app.briefly.co.stage.systools.pro/v2/post/deleted
    Then the http status code is 200
    And the body matches the following Json Schema in classpath deleted-schema.json
