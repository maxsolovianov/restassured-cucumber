@redirect
Feature: Control following of redirects: assert the correct location of the redirect; check redirect to public URL

  @nur
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 1739861
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service https://app.nur.stage.systools.pro/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://www.nur.stage.systools.pro/1739861-eks-akima-osudili-po-delu-o-vystrele-v-podrostka-v-uralske.html
    When a user go to https://www.nur.stage.systools.pro/1739861-eks-akima-osudili-po-delu-o-vystrele-v-podrostka-v-uralske.html
    Then the http status code is 200

  @naij @production
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 1102748
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service http://app.naij.com/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://app.naija.ng/v3/post/redirect?id=1102748
    When a user retrieves the data from service https://app.naija.ng/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://www.naija.ng/1102748-11-never-put-facebook.html
    When a user go to https://www.naija.ng/1102748-11-never-put-facebook.html
    Then a user retrieves the html document includes the following values
      | html.head.title                                                  | 11 things you must NEVER put on your Facebook ▷ NAIJA.NG |
      | **.findAll { it.@class == 'article-post__author io-author' }     | Author:Chika Jones                                       |
      | **.findAll { it.@class == 'article-post__category io-category' } | Category:Local newsLife and StyleGossip                  |
      | **.findAll { it.@class == 'share-article__caption' }             | Think it is important? Share with your friends!          |

  @yen @ignore
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 112024
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service https://app.yen.stage.systools.pro/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://yen.stage.systools.pro/112024-owusu-bempah-supports-kennedy-agyapong-fight-anas.html
    When a user go to https://yen.stage.systools.pro/112024-owusu-bempah-supports-kennedy-agyapong-fight-anas.html
    Then the http status code is 200
    Then a user retrieves the html document includes the following values
      | html.head.title                                                  | Owusu Bempah supports Kennedy Agyapong in fight against Anas ▷ YEN.COM.GH |
      | **.findAll { it.@class == 'article-post__author io-author' }     | Author:Jeffrey Mensah                                                     |
      | **.findAll { it.@class == 'article-post__category io-category' } | Category:PoliticsLocal                                                    |
      | **.findAll { it.@class == 'share-article__caption' }             | Think it is important? Share with your friends!                           |

  @tuko_news @ignore
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 23045
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service https://app.tuko.news.stage.systools.pro/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://www.tuko.news.stage.systools.pro/23045-lobby-group-writes-uhuru-dpp-suspicious-british-lawyer-guy-spencer-elms.html
    When a user go to https://www.tuko.news.stage.systools.pro/23045-lobby-group-writes-uhuru-dpp-suspicious-british-lawyer-guy-spencer-elms.html
    Then the http status code is 200
    Then a user retrieves the html document includes the following values
      | html.head.title                                              | Lobby group writes to Uhuru, DPP over suspicious British lawyer Guy Spencer Elms |
      | **.findAll { it.@class == 'article-post__author io-author' } | Author:Asher Omondi                                                              |
      | **.findAll { it.@class == 'share-article__caption' }         | Think it is important? Share with your friends!                                  |

  @tuko @ignore
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 278090
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service https://app.tuko.co.stage.systools.pro/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://kiswahili.tuko.co.stage.systools.pro/278090-abiria-mwingine-afariki-baada-ya-makanga-kumtupa-nje-ya-gari-mlolongo.html
    When a user go to https://kiswahili.tuko.co.stage.systools.pro/278090-abiria-mwingine-afariki-baada-ya-makanga-kumtupa-nje-ya-gari-mlolongo.html
    Then the http status code is 200
    Then a user retrieves the html document includes the following values
      | html.head.title                                              | Abiria mwingine afariki baada ya makanga kumtupa nje ya gari Mlolongo ▷ Tuko.co.ke |
      | **.findAll { it.@class == 'article-post__author io-author' } | Mwandishi:Joshua Kithome                                                           |
      | **.findAll { it.@class == 'share-article__caption' }         | Unaona infaa? Wajulishe marafiki!                                                  |

  @kami @ignore
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 78079
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service https://app.kami.stage.systools.pro/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://kami.stage.systools.pro/78079-netizens-accuse-sharon-cuneta-a-show-sharing-story-rolex-watches.html
    When a user go to https://kami.stage.systools.pro/78079-netizens-accuse-sharon-cuneta-a-show-sharing-story-rolex-watches.html
    Then the http status code is 200
    Then a user retrieves the html document includes the following values
      | html.head.title                                              | Netizens accuse Sharon Cuneta of being a show-off after sharing story of her Rolex watches ▷ KAMI.COM.PH |
      | **.findAll { it.@class == 'article-post__author io-author' } | Author:Daniel Joseph Navalta                                                                             |
      | **.findAll { it.@class == 'share-article__caption' }         | Think it is important? Share with your friends!                                                          |

  @briefly @ignore
  Scenario: Control following of redirects: redirect to public URL
    Given a news exists with an id of 13491
    And I won't follow a redirect for getting response of the original request
    When a user retrieves the data from service https://app.briefly.co.stage.systools.pro/v3/post/redirect
    Then the http status code is 301
    And the header Location contains https://briefly.co.stage.systools.pro/13491-twitter-fans-excited-mamiki-leaves-the-queen.html
    When a user go to https://briefly.co.stage.systools.pro/13491-twitter-fans-excited-mamiki-leaves-the-queen.html
    Then the http status code is 200
    Then a user retrieves the html document includes the following values
      | html.head.title                                              | Twitter fans excited after Mamiki leaves The Queen |
      | **.findAll { it.@class == 'article-post__author io-author' } | Author:Noncedo Dlamini                             |
      | **.findAll { it.@class == 'share-article__caption' }         | Think it is important? Share with your friends!    |