@trending
Feature: Get no more than 5 trending posts

  @nur
  Scenario: User calls web service (nur) to get no more than 5 trending posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.nur.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 50     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
      | lang         | ru     |
    Then the http status code is 200
    And response includes no more than 5 trending posts

  @naij
  Scenario: User calls web service (naij) to get no more than 5 trending posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.naija.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 50     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And response includes no more than 5 trending posts

  @yen
  Scenario: User calls web service (yen) to get no more than 5 trending posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.yen.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 50     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And response includes no more than 5 trending posts

  @tuko
  Scenario: User calls web service (tuko) to get no more than 5 trending posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.tuko.co.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 50     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And response includes no more than 5 trending posts

  @kami
  Scenario: User calls web service (kami) to get no more than 5 trending posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.kami.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 50     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And response includes no more than 5 trending posts

  @briefly
  Scenario: User calls web service (briefly) to get no more than 5 trending posts
    Given a posts exists on server
    When a user retrieves the posts with using such parameters from https://app.briefly.co.stage.systools.pro/v3/post/list
      | category     | latest |
      | per-page     | 50     |
      | short        | 0      |
      | offsetPostId | 0      |
      | showTrending | 1      |
    Then the http status code is 200
    And response includes no more than 5 trending posts
