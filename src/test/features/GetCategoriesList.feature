@categories
Feature: Get categories list

  @nur
  Scenario: User calls web service NUR to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.nur.stage.systools.pro/categories
    Then the http status code is 200
    And response includes the following list by key 'name'
      | Последние            |
      | Общество             |
      | Политика и Экономика |
      | Происшествия         |
      | В мире               |
      | Шоу-бизнес           |
      | Калейдоскоп          |
      | Красота и Здоровье   |
#    And response includes the following list by key 'slug'
#      | latest                 |
#      | politics               |
#      | society                |
#      | ekonomika-zhane-biznes |
#      | sport                  |
#      | showbiz                |
#      | health                 |
#      | accidents              |

  @nur
  Scenario: User calls web service NUR to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.nur.stage.systools.pro/categories
    Then the body matches the following Json Schema in classpath categories-schema.json

  @naij
  Scenario: User calls web service NAIJ to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.naija.stage.systools.pro/categories
    Then the http status code is 200
    And response includes the following list by key 'name'
      | Latest               |
      | Photo                |
      | Business and Economy |
      | Education news       |
      | Featured news        |
      | Football News        |
      | World News           |
      | Life and Style       |
      | Local news           |
      | Opinion              |
      | Politics             |
      | Gossip               |
      | Wedding news         |
    And response includes the following list by key 'slug'
      | latest               |
      | photonews            |
      | business_and_economy |
      | education            |
      | features             |
      | football-news        |
      | world-news           |
      | life-and-style       |
      | local                |
      | opinion              |
      | politics             |
      | gossip               |
      | weddings             |

  @naij
  Scenario: User calls web service NAIJ to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.naija.stage.systools.pro/categories
    Then the body matches the following Json Schema in classpath categories-schema.json

  @yen
  Scenario: User calls web service YEN to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.yen.stage.systools.pro/categories
    Then the http status code is 200
    And response includes the following list by key 'name'
      | Latest              |
      | Politics            |
      | Finance             |
      | Entertainment       |
      | Sport               |
      | Photo               |
      | Health and Wellness |
      | Crime               |
      | Education           |

  @yen
  Scenario: User calls web service YEN to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.yen.stage.systools.pro/categories
    Then the body matches the following Json Schema in classpath categories-schema.json

  @tuko
  Scenario: User calls web service TUKO to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.tuko.co.stage.systools.pro/categories
    Then the http status code is 200
    And response includes the following list by key 'name'
      | Latest              |
      | Politics            |
      | Business            |
      | Entertainment       |
      | Sport               |
      | Photo               |
      | Health and Wellness |
      | Crime               |
      | Education           |
      | Buzz                |

  @tuko
  Scenario: User calls web service TUKO to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.tuko.co.stage.systools.pro/categories
    Then the body matches the following Json Schema in classpath categories-schema.json

  @kami
  Scenario: User calls web service KAMI to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.kami.stage.systools.pro/categories
    Then the http status code is 200
    And response includes the following list by key 'name'
      | Latest                         |
      | Mga Balita Ngayon sa Pilipinas |
      | Political News                 |
      | Entertainment                  |
      | World News                     |
      | Business and Economy           |
      | Sports                         |
      | Education and Technology       |
      | Opinion                        |
      | Life and Style                 |
      | Photos                         |
      | Spirituality                   |

  @kami
  Scenario: User calls web service KAMI to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.kami.stage.systools.pro/categories
    Then the body matches the following Json Schema in classpath categories-schema.json

  @briefly
  Scenario: User calls web service BRIEFLY to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.briefly.co.stage.systools.pro/categories
    Then the http status code is 200
    And response includes the following list by key 'name'
      | Latest               |
      | Entertainment        |
      | Mzansi news          |
      | Lifestyle            |
      | Facts and Life Hacks |
      | Politics             |
      | Buzz                 |
      | Sports               |

  @briefly
  Scenario: User calls web service BRIEFLY to get a categories list
    Given a list of categories exists on server
    When a user retrieves the data from service https://app.briefly.co.stage.systools.pro/categories
    Then the body matches the following Json Schema in classpath categories-schema.json
